module.exports = {
  checkPermissions: (userBadges, commandPermissions) => {
    if (commandPermissions[0] === 'everyone') {
      return true;
    }
    for (p=0; p<userBadges.length; p++) {
      if (commandPermissions.indexOf(userBadges[p])) {
        return true;
      }
    }
    return false;
  }
}