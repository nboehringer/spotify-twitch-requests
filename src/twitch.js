const tmi = require('tmi.js');
var twitchBot;

requests=[];

function onChatHandler(channel, userstate, message, self) {
  if (self) return
    // Get the command
    var command = message.replace(/ .*/, '');
    var args = message.replace(/[^\s]+ /, '').split(' ');

    switch (command) {
      case "!sr":
        if (args && args[0] === command) {
          twitchBot.say(channel, 'Usage: ' + command + ' {song name}');
          break;
        }
        searchData = args.join(' ');
        spotify.search(searchData, (data) => {
          if (!data || !data.uri) {
            twitchBot.say(channel, 'You muppet, That song does\'t exist');
          } else {
            twitchBot.say(channel, 'Adding ' + data.artists[0].name + ' - ' + data.name + ' to song queue');
            requests.push({
              username: userstate.username,
              songuri: data.uri,
              songName: data.artists[0].name + ' - ' + data.name
            })
            spotify.addTrack(data.uri, (playlist) => {
              console.log('Track: ' + data.artists[0].name + ' - ' + data.name + ' Added to playlist ');
            });
          }
        })
        break;
      case "!song":
        spotify.getPlayerDetails((response) => {
          if (response && response.item && response.item.album && response.item.album.artists) {
            artist = response.item.album.artists[0].name;
          }
          song = response.item.name;
          twitchBot.say(channel, "Currently playing: " + song + " by " + artist);
        })
        break;
    }
}

//setInterval(() => {
//  console.log(requests);
//}, 60000)

module.exports = {
  init: () => {
   twitchBot = new tmi.client(global.config.twitch.twitchBot) ;
   twitchBot.connect();

   twitchBot.on('connected', () => {
     twitchBot.say(global.config.twitch.twitchBot.channels[0], global.application.displayName + ' (' + global.application.version + ') is connected');
   })

   twitchBot.on('chat', onChatHandler);
  }
}