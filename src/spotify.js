var express = require('express');
var querystring = require('querystring');
var request = require('request');

var base_url = 'https://api.spotify.com/v1/';
var myBaseUrl = 'http://localhost:8888/'

var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};


module.exports = {
  init:() => {
  if (!global.config.spotify || !global.config.spotify.access_token) {
  var app = express();

  var scope = 'user-read-private user-read-email playlist-read-private playlist-modify-private user-read-playback-state user-modify-playback-state streaming'; 
  app.get('/', function(req, res) {
    res.redirect('https://accounts.spotify.com/authorize?' +
      querystring.stringify({
        response_type: 'code',
        client_id: process.env.SPOTIFY_CLIENT_ID,
        scope: scope,
        redirect_uri: myBaseUrl + 'spotify-callback',
        state: generateRandomString(16)
      })
    );
  })

  app.get('/spotify-callback', function (req, res) {
    var client_id = process.env.SPOTIFY_CLIENT_ID;
    var client_secret = process.env.SPOTIFY_CLIENT_SECRET;
    var code = req.query.code;
    var  authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: myBaseUrl + 'spotify-callback',
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true
    }
    request.post(authOptions, function(error, response, body) {
      if (error) {
        console.log(error);
      }
      var seconds = Math.floor(new Date().getTime() / 1000);
      seconds = seconds + parseInt(body.expires_in);
      global.config.spotify = {
        access_token: body.access_token,
        refresh_token: body.refresh_token,
        expires_at: seconds
      }

      module.exports.api({
        path: 'me',
        type: 'get'
      }, (data) => {
        global.config.spotify.userid = data.id;

        module.exports.api({
          path: 'me/playlists',
          type: 'get'
        }, (playlists) => {
          if (playlists.items) {
            for (i=0; i<playlists.items.length; i++) {
              if (playlists.items[i].name === "SongRequests") {
                global.config.spotify.playlistId = playlists.items[i].id;
              }
            }
            if (!global.config.spotify.playlistId) {
              module.exports.api({
                path: 'users/' + global.config.spotify.userid +'/playlists',
                type: 'post',
                formData: {
                  name: 'SongRequests',
                  public: false
                }
              }, (newplaylist) => {
                global.config.spotify.playlistId = newplaylist.id
                global.writeConfig('spotify');
                console.log('Using spotify playlist id ' + global.config.spotify.playlistId);
              })
            } else {
                global.writeConfig('spotify');
                console.log('Using spotify playlist id ' + global.config.spotify.playlistId);
            }
          }
        })

        
      })


      res.send('Authentication complete - You may close this window');
    })
    
  })
  app.listen(8888);
  }
  },
  api: async (apiRequest, c) => {
    var seconds = Math.floor(new Date().getTime() / 1000);
    if (global.config.spotify.expires_at < seconds) {
      token = await module.exports.refreshToken();
      if (!token) {
        return false;
      }
    }
    var options = {
      url: base_url + apiRequest.path,
      headers: {
        'Authorization': 'Bearer ' + global.config.spotify.access_token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    }

    switch (apiRequest.type) {
      case "post":
        options.form = JSON.stringify(apiRequest.formData);
        options.method = 'POST';
        request.post(options, ( err, res, body) => {
          c(JSON.parse(body));
        })
        break;
      case "get":
        options.method = 'GET';
        request(options, ( err, res, body) => {
          if(err) {
            console.log(err);
          }
          if (body) {
            c(JSON.parse(body));
          } else {
            c(false);
          }
        })

        break;
      case "put":
        if (apiRequest.formData) {
          options.form = JSON.stringify(apiRequest.formData);
        }
        options.method = 'PUT';
        request.put(options, ( err, res, body) => {
          if (res && res.statusCode && res.statusCode === 204) {
            c(true);
          } else {
          c(JSON.parse(body));
          }
        })
        break;
      case "delete":
        if (apiRequest.formData) {
          options.form = JSON.stringify(apiRequest.formData);
        }
        options.method = 'DELETE';
        request.delete(options, ( err, res, body) => {
          c(JSON.parse(body));
        })
        break;
    }
  },
  search: (searchQuery, c) => {
    apiRequest = {
      path: 'search?q=' + searchQuery +'&type=track',
      type: 'get'
    }
    module.exports.api(apiRequest, (data) => {
//      for (i=0; i<data.tracks.items; i++) {
//        console.log(data.tracks.items[i]);
//      }
      c(data.tracks.items[0]);
    })
  },
  addTrack(uri, c) {
    module.exports.api({
      path: 'playlists/' + global.config.spotify.playlistId + '/tracks',
      type: 'post',
      formData: { uris: [uri]}
    }, (data) => {
      c(data);
    })
  },
  refreshToken: () => {
    request.post({
      url: 'https://accounts.spotify.com/api/token',
      headers: {
        'Authorization': 'Basic ' + (new Buffer(process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET).toString('base64')),
        'Accept': 'application/json'
      },
      form: {
        grant_type: 'refresh_token',
        refresh_token: global.config.spotify.refresh_token
      }
    }, (err, response, body) => {
      if (err) {
        console.log(err)
        return false;
      }
      data = JSON.parse(body);
      if (data && data.access_token) {
        var seconds = Math.floor(new Date().getTime() / 1000);
        seconds = seconds + data.expires_in;
        global.config.spotify.access_token = data.access_token;
        global.config.spotify.expires_at = seconds;
        global.writeConfig('spotify');
        return body.access_token;
      } else  {
        return false;
      }

    })
  },
  getPlayerDetails:(c) => {
    module.exports.api({
      path: 'me/player',
      type: 'get'
    }, (data) => {
      c(data);
    })
  },
  getPlaylist: (playlist, c) => {
    module.exports.api({
      path: 'playlists/' + playlist + '/tracks',
      type: 'get'
    }, (result) => {
      c(result);
    })
  },
  setPlaying: (context, c) => {
    module.exports.api({
      path: 'me/player/play',
      type: 'put',
      formData: {
        context_uri: context
      }
    }, (ret) => {
      if (ret) {
        c(ret);
      } 
    })
  },
  removeFromPlaylist: (playlist, trackuri, c) => {
    module.exports.api({
      path: 'playlists/' + playlist + '/tracks',
      type: 'delete',
      formData: {
        tracks: [
          {
            uri: trackuri
          }
        ]
      }
    }, (ret) => {
      if (ret) {
        c(ret);
      }
    })
  },
  toggleShuffle: (state, c) => {
    if (!state) {
      state=false;
    }
    console.log('Setting Shuffle State');
    module.exports.api({
      path: 'me/player/shuffle?state=' + state,
      type: 'put',
    }, (res) => {
      if (c) {
        c(res);
      }
    })
  }
}
