const dotenv = require('dotenv');
const conf= require('./config');
const twitch = require('./twitch')
const logger = require('./logger');
const open = require('open');
global.spotify = require('./spotify');
global.writeConfig = conf.storeConfig;

logger.logApplicationInfo();
dotenv.config();

var pollTimer;
var changeTimer;
var trackChangeTime;
var currentPlaying;
var playlistInitTimer;
var fallbackPlaylist;

function checkPlaylist() {
  console.log('Checking Playlist')
  // clear the timer
  clearTimeout(changeTimer);
  clearTimeout(pollTimer);
  spotify.getPlayerDetails((response) => {
    if (!response) {
      console.log('Couldn\'t get player details, is spotify running?');
      console.log('Retrying in 10 seconds...');
      setTimeout(() => {
        checkPlaylist();
      },10000);
      return false;
    } else {
      // get the current track playtime
      if (!response.item) {
        console.log('Unable to detect current track, retrying in 10 seconds');
        setTimeout(() => {
          checkPlaylist();
        },10000);
        return false;
      }
      trackChangeTime = (parseInt(response.item.duration_ms) - parseInt(response.progress_ms));
      console.log('Switching tracks in ' + trackChangeTime + ' ms')
      // current context
      currentPlaying = response.context.uri;
      thisTrack = response.item.uri;
      // Set a timer to change tracks
      changeTimer = setTimeout(() => {
        clearTimeout(pollTimer);
        // change track
        if(currentPlaying.replace(/.*:playlist:/, '') === global.config.spotify.playlistId) {
          //remove the current track
          console.log('Removing Song From Playlist')
          spotify.removeFromPlaylist(global.config.spotify.playlistId, thisTrack, (d) => {
            //check requests
          console.log('Getting Playlist')
            spotify.getPlaylist(global.config.spotify.playlistId, (ret) => {
              if (ret && ret.items && ret.items.length === 0) {
                //no more requests
                console.log('Falling back to backup playlist')
                spotify.toggleShuffle(true, () => {
                  spotify.setPlaying(fallbackPlaylist, (ret) => {
                    checkPlaylist();
                  });
                });
              } else {
                  checkPlaylist();
              }
            });
          });
        } else {
          fallbackPlaylist = response.context.uri;
          // not playing from the request queue
          console.log('Getting Playlist')
            spotify.getPlaylist(global.config.spotify.playlistId, (ret) => {
              if (ret && ret.items && ret.items.length > 0) {
                //We have a request
                console.log('Changing to Requests')
                spotify.toggleShuffle(false, () => {
                  spotify.setPlaying('spotify:playlist:' + global.config.spotify.playlistId, (d) => {
                    checkPlaylist();
                  });
                });
              } else {
                  checkPlaylist();
              }
            });
        }
      }, trackChangeTime)
    }
  });
  pollTimer = setTimeout(() => {
    checkPlaylist();
  },15000)
}

async function main () {
  global.config = await conf.getConfig();
  currentSeconds = Math.floor(new Date().getTime() /1000);
  spotify.init();
  twitch.init();
  if (global.config && global.config.spotify && global.config.spotify.expires_at) {
    if (global.config.spotify.expires_at < currentSeconds) {
     await spotify.refreshToken();
    }
//    spotify.getPlayerDetails((response) => {
//      console.log(response);
//    });
  } else {
    open('http://localhost:8888/');
  }
  playlistInitTimer = setInterval(() =>{ 
    if (global.config && global.config.spotify && global.config.spotify.access_token) {
      console.log('Clearing Timer');
      clearInterval(playlistInitTimer);
      checkPlaylist();
    }
  }, 1000)
}


main();