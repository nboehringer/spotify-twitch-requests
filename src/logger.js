const os = require('os');
const { name, displayName, version } = require('../package.json');

module.exports = {
  logApplicationInfo: () => {
    var application = {
        name,
        version: version,
        displayName
    }
    global.application = application;
    return true
  }
}